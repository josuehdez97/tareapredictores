Estudiante: Josué Hernández Calderón
Carné: B63326
Programa: Algoritmos de predictores de salto: Bimodal, GShare, PShare y de Torneo.
El programa tiene como parámetros de entrada:
1. Tamaño de la tabla BTH. (-s): numero
2. Tipo de predicción (-bp): String "Bimodal", "GH", "PH" o "Torneo".
3. Tamaño del registro de predicción global (-gh): numero
4. Tamaño de los registros de historia privada (-ph): numero
5. Salida de la simulación (-o): Si es diferente a 1 no crea el archivo de resultados, de lo contrario si lo hace, con el nombre del predictor usado.


Compilacion:
g++ -o branch Predictores.cpp

Ejecución para Bimodal:
	gunzip -c branch-trace-gcc.trace.gz | ./branch -s < # > -bp Bimodal -gh < # > -ph < # > -o < # >
Ejecución para GShare:
	gunzip -c branch-trace-gcc.trace.gz | ./branch -s < # > -bp GShare -gh < # > -ph < # > -o < # >
Ejecución para PShare:
	gunzip -c branch-trace-gcc.trace.gz | ./branch -s < # > -bp PShare -gh < # > -ph < # > -o < # >
Ejecución para Torneo:
	gunzip -c branch-trace-gcc.trace.gz | ./branch -s < # > -bp Torneo -gh < # > -ph < # > -o < # >
	
Como ejecutar para N lineas de archivo:
	gunzip -c branch-trace-gcc.trace.gz | head -N| ./brach -s < # > -bp < "NombreDelPredictor" > -gh < # > -ph < # > -o < # >
	Donde N son las lineas que se desean procesar del trace.
	
Dependencias:
	Se debe contar con el archivo trace "branch-trace-gcc.trace.gz" en la misma carpeta que el códico fuente "Predictores.cpp" 
