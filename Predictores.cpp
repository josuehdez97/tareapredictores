//Estudiante: Josué Hernández Calderón
//Carné: B63326
//Programa: Algoritmos de predictores de salto: Bimodal, GShare, PShare y de Torneo.
// El programa tiene como parámetros de entrada:
//1. Tamaño de la tabla BTH. (-s): numero
//2. Tipo de predicción (-bp): String "Bimodal", "GH", "PH" o "Torneo".
//3. Tamaño del registro de predicción global (-gh): numero
//4. Tamaño de los registros de historia privada (-ph): numero
//5. Salida de la simulación (-o): Si es diferente a 1 no crea el archivo de resultados, de lo contrario si lo hace, con el nombre del predictor usado.


#include <iostream> 
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <fstream>

using namespace std;  


//Funcion para el predictor Bimodal: 
//Se utiliza un BHT de tamaño 2^s.
//Cada entrada del BHT es un arreglo de 2 bits.
//El manejo de los numeros se hace en decimal para evitar convertir a binario y que eso implique mayor numero de instrucciones.
//Entradas del BHT:
//0(00) : Strongly not Taken
//1(01) : Weakly not Taken
//2(10) : Weakly Taken
//3(11) : Strongly Taken
void Bimodal (string s, string gh, string ph, string o){
	int TamanoBHT = pow(2,std::stoi(s)); //Tamaño de BHT
	int BHT [TamanoBHT] = {};	//Se inicializa en 0 (Strongly not Taken)
	int Entrada_BHT;
	int aciertosN = 0;	//Correct prediction of not taken branches:
	int aciertosT = 0;	//Correct prediction of taken branches:
	int fallosN = 0;	//Incorrect prediction of not taken branches:
	int fallosT = 0; 	//Incorrect prediction of taken branches:
	string prediccion;
	unsigned long int PC; 
	char cadena [100]="a";			//Cadena almacenará los datos del archivo de entrada para irlos analizando
	ofstream file;
	file.open("Bimodal.txt");
	file << "PC\t\t\tOutcome\tPrediction\tcorrect/incorrect\n";
	cin >> cadena;	//Se lee el PC
	while (!cin.eof()){
		file << cadena << "\t"; //Se escribe el PC
		PC=strtoul(cadena,NULL,10);	//Convierte el PC a unsigned long int
		Entrada_BHT = (TamanoBHT-1) & PC; //Se aplica el TamañoBHT como máscara para los primeros n bits del PC. Esta variable permite saber donde indexar en la BHT.
		cin >> cadena;		//Se lee el outcome
		file << cadena << "\t\t";			//Se escribe el outcome
		if (0 == BHT[Entrada_BHT]){			//Si es Strongly not Taken
			prediccion = "N";
			file << prediccion << "\t\t\t";		//Se escribe la prediccion
			if (prediccion == cadena){
				file << "correct\n";
				aciertosN++;
			}else{
				file << "incorrect\n";
				BHT[Entrada_BHT] = 1;		//Aumenta a Weakly not Taken
				fallosT++;
			}	
		}
		else if (1 == BHT[Entrada_BHT]){		//Si es Weakly not Taken
			prediccion = "N";
			file << prediccion << "\t\t\t";
			if (prediccion == cadena){
				file << "correct\n";
				BHT[Entrada_BHT] = 0;			//Disminuye a Strongly not Taken
				aciertosN++;
			}else{
				file << "incorrect\n";
				BHT[Entrada_BHT] = 2;		//Aumenta a Weakly Taken
				fallosT++;
			}	
		}
		else if (2 == BHT[Entrada_BHT]){		//Si es Weakly Taken
			prediccion = "T";
			file << prediccion << "\t\t\t";
			if (prediccion == cadena){
				file << "correct\n";
				BHT[Entrada_BHT] = 3;		//Aumenta a Strongly Taken
				aciertosT++;
			}else{
				file << "incorrect\n";
				BHT[Entrada_BHT] = 1;		//Disminuye a Weakly Taken
				fallosN++;
			}	
		}
		else if (3 == BHT[Entrada_BHT]){		//Si es Strongly Taken
			prediccion = "T";
			file << prediccion << "\t\t\t";
			if (prediccion == cadena){
				file << "correct\n";
				aciertosT++;
			}else{
				file << "incorrect\n";
				BHT[Entrada_BHT] = 2;		//Disminuye a Weakly Taken
				fallosN++;
			}	
		}
		cin >> cadena;				// Lee el siguiente PC
	}
	file.close();
	
	int Aciertos = aciertosN + aciertosT;
	int Branches = aciertosN + fallosN + aciertosT + fallosT;
	float PorcentajeAciertos = (float(Aciertos)/float(Branches))*100;
	if (o!="1")
		remove("Bimodal.txt");
	//Imprime tabla de resultados:
	cout << "\n\nPrediction parameters:\nBranch prediction type:\t\t\t\t\tBimodal\nBHT size (entries):\t\t\t\t\t" << TamanoBHT << "\nGlobal history register size:\t\t\t\t" << gh << "\nPrivate history register size:\t\t\t\t" << ph <<"\nSimulation results:\nNumber of branch:\t\t\t\t\t" << Branches  << "\nNumber of correct prediction of taken branches:\t\t" << aciertosT << "\nNumber of incorrect prediction of taken branches:\t" << fallosT << "\nCorrect prediction of not taken branches:\t\t" << aciertosN << "\nIncorrect prediction of not taken branches:\t\t" << fallosN << "\nPercentage of correct predictions\t\t\t" << PorcentajeAciertos << "\n\n";
}

//Funcion para el predictor de Historia Global: 
//Se utiliza un BHT de tamaño 2^s.
//Cada entrada del BHT es un arreglo de 2 bits.
//El manejo de los numeros se hace en decimal para evitar convertir a binario y que eso implique mayor numero de instrucciones.
//Entradas del BHT:
//0(00) : Strongly not Taken
//1(01) : Weakly not Taken
//2(10) : Weakly Taken
//3(11) : Strongly Taken
//La historia es un unsigned long int que se iniciliza en 0 y gh bits.
void GShare (string s, string gh, string ph, string o){
	unsigned long int TamanoHistoria = pow(2,std::stoi(gh))-1;		//Es una máscara para limitar el tamaño de la historia a gh bits
	unsigned long int Historia =0; 
	unsigned long int TamanoBHT = pow(2,std::stoi(s));		// Tamaño del BHT
	int BHT [TamanoBHT]= {}; 			//Se inicializa en 0
	unsigned long int PC;
	unsigned long int Entrada_BHT;		//Entrada para indexar la BHT
	int aciertosN = 0;	//Correct prediction of not taken branches:
	int aciertosT = 0;	//Correct prediction of taken branches:
	int fallosN = 0;	//Incorrect prediction of not taken branches:
	int fallosT = 0; 	//Incorrect prediction of taken branches:
	string prediccion;
	char cadena [100]="a";		//Cadena almacenará los datos del archivo de entrada para irlos analizando
	ofstream file;
	file.open("GShare.txt");
	file << "PC\t\t\tOutcome\tPrediction\tcorrect/incorrect\n";
	cin >> cadena;			//Se lee el PC
	while (!cin.eof()){
		file << cadena << "\t";		//Se escribe el PC en el archivo
		PC=strtoul(cadena,NULL,10);		//Convierte el PC a unsigned long int
		Entrada_BHT = ((Historia ^ PC) & (TamanoBHT-1));	//Esta operacion define la entrada a indexar de la BHT. Es un Xor del PC y la historia y se aplica como máscara TamanoBHT-1 para limitar la cantidad de bit de acuerdo al tamaño de la tabla.
		Historia = Historia << 1;		//Shift a la izquierda un espacio
		Historia = Historia & TamanoHistoria;	//Máscara para el bit que aumento debido al shift
		cin >> cadena;			//Se lee el Outcome
		file << cadena << "\t\t";		//Se escribe el Outcome
		
		if (0 == BHT[Entrada_BHT]){		//Si es Strongly not Taken
			prediccion = "N";
			file << prediccion << "\t\t\t";
			if (prediccion == cadena){	//Si se acertó la predicción
				file << "correct\n";
				aciertosN++;
			}else{					
				file << "incorrect\n";
				BHT[Entrada_BHT] = 1;		//Aumenta a Weakly not Taken
				fallosT++;
				Historia++;					//Se suma 1 a la historia porque el salto fue Taken
			}
		}
		else if (1 == BHT[Entrada_BHT]){		//Si es Weakly not Taken
			prediccion = "N";
			file << prediccion << "\t\t\t";
			if (prediccion == cadena){		//Si se acertó la predicción
				file << "correct\n";
				BHT[Entrada_BHT] = 0;			//Disminuye a Strongly not Taken	
				aciertosN++;
			}else{
				file << "incorrect\n";
				BHT[Entrada_BHT] = 2;		//Aumenta a Weakly Taken
				fallosT++;
				Historia++;			//Se suma 1 a la historia porque el salto fue Taken
			}	
		}
		else if (2 == BHT[Entrada_BHT]){		//Si es Weakly Taken
			prediccion = "T";
			file << prediccion << "\t\t\t";
			if (prediccion == cadena){		//Si se acertó la predicción
				file << "correct\n";
				BHT[Entrada_BHT] = 3;		//Aumenta a Strongly Taken
				aciertosT++;
				Historia++;			//Se suma 1 a la historia porque el salto fue Taken
			}else{
				file << "incorrect\n";
				BHT[Entrada_BHT] = 1;		//Disminuye a Weakly not Taken	
				fallosN++;
			}	
		}
		else if (3 == BHT[Entrada_BHT]){		//Si es Strongly Taken
			prediccion = "T";
			file << prediccion << "\t\t\t";
			if (prediccion == cadena){		//Si se acertó la predicción
				file << "correct\n";
				aciertosT++;
				Historia++;			//Se suma 1 a la historia porque el salto fue Taken
			}else{
				file << "incorrect\n";
				BHT[Entrada_BHT] = 2;			//Disminuye a Weakly Taken	
				fallosN++;
			}	
		}
		cin >> cadena;			//Se lee el siguiente PC
	}
	file.close();
	
	int Aciertos = aciertosN + aciertosT;
	int Branches = aciertosN + fallosN + aciertosT + fallosT;
	float PorcentajeAciertos = (float(Aciertos)/float(Branches))*100;
	if (o!="1")
		remove("GShare.txt");
	
	cout << "\n\nPrediction parameters:\nBranch prediction type:\t\t\t\t\tGShare\nBHT size (entries):\t\t\t\t\t" << TamanoBHT << "\nGlobal history register size:\t\t\t\t" << gh << "\nPrivate history register size:\t\t\t\t" << ph <<"\nSimulation results:\nNumber of branch:\t\t\t\t\t" << Branches  << "\nNumber of correct prediction of taken branches:\t\t" << aciertosT << "\nNumber of incorrect prediction of taken branches:\t" << fallosT << "\nCorrect prediction of not taken branches:\t\t" << aciertosN << "\nIncorrect prediction of not taken branches:\t\t" << fallosN << "\nPercentage of correct predictions\t\t\t" << PorcentajeAciertos << "\n\n";
}

//Funcion para el predictor de Historia Privada: 
//Se utiliza un BHT de tamaño 2^s.
//Cada entrada del BHT es un arreglo de 2 bits.
//El manejo de los numeros se hace en decimal para evitar convertir a binario y que eso implique mayor numero de instrucciones.
//Entradas del BHT:
//0(00) : Strongly not Taken
//1(01) : Weakly not Taken
//2(10) : Weakly Taken
//3(11) : Strongly Taken
//La PHT (Private History Table) es un arreglo de tamaño 2^s. Y cada es un registro de 2^(ph) bits.
void PShare (string s, string gh, string ph, string o){
	unsigned long int TamanoHistoria = pow(2,std::stoi(ph));
	unsigned long int TamanoBHT = pow(2,std::stoi(s)); //Tambien funciona para indexar el BHT
	int BHT [TamanoBHT]= {};	//Se inicilizan en 0
	int PHT [TamanoBHT]= {};
	unsigned long int PC;
	unsigned long int Entrada_BHT;
	unsigned long int Entrada_PHT;
	int aciertosN = 0;	//Correct prediction of not taken branches:
	int aciertosT = 0;	//Correct prediction of taken branches:
	int fallosN = 0;	//Incorrect prediction of not taken branches:
	int fallosT = 0; 	//Incorrect prediction of taken branches:
	string prediccion;
	char cadena [100]="a";
	ofstream file;
	file.open("PShare.txt");
	file << "PC\t\t\tOutcome\tPrediction\tcorrect/incorrect\n";
	cin >> cadena;		//PC
	while (!cin.eof()){
		file << cadena << "\t";
		PC=strtoul(cadena,NULL,10);
		Entrada_PHT = (TamanoBHT-1) & PC;		//La entrada a indexar del PHT son los ultimos n bits del PC. Para ello se aplica la máscara TamanoBHT-1.
		Entrada_BHT = (PHT[Entrada_PHT] ^ Entrada_PHT) & (TamanoBHT-1);		//La entrada para indexar el BHT es un Xor de la entrada del PHT y el dato en esa posicion
		PHT[Entrada_PHT] = PHT[Entrada_PHT] << 1;	//Se desplaza a la izquierda  la el valor en la PHT
		PHT[Entrada_PHT] = PHT[Entrada_PHT] & (TamanoHistoria-1);		//Se aplica una máscara para eliminar el MSB desplazado
		cin >> cadena;		//Outcome
		file << cadena << "\t\t";
		if (0 == BHT[Entrada_BHT]){		//Si Strongly Not Taken
			prediccion = "N";
			file << prediccion << "\t\t\t";
			if (prediccion == cadena){
				file << "correct\n";
				aciertosN++;
			}else{
				file << "incorrect\n";
				BHT[Entrada_BHT] = 1;		//Aumenta a Weakly Not Taken
				fallosT++;
				PHT[Entrada_PHT]++;		// Suma 1 si el salto se tomó
			}	
		}
		else if (1 == BHT[Entrada_BHT]){		//Si Weakly Not Taken
			prediccion = "N";
			file << prediccion << "\t\t\t";
			if (prediccion == cadena){
				file << "correct\n";
				BHT[Entrada_BHT] = 0;		//Disminuye a Strongly Not Taken
				aciertosN++;
			}else{
				file << "incorrect\n";
				BHT[Entrada_BHT] = 2;		//Aumenta a Weakly Taken
				fallosT++;
				PHT[Entrada_PHT]++;		// Suma 1 si el salto se tomó
			}	
		}
		else if (2 == BHT[Entrada_BHT]){		//Si Weakly Taken
			prediccion = "T";
			file << prediccion << "\t\t\t";
			if (prediccion == cadena){
				file << "correct\n";
				BHT[Entrada_BHT] = 3;		//Aumenta a Weakly Taken
				aciertosT++;
				PHT[Entrada_PHT]++;		// Suma 1 si el salto se tomó
			}else{
				file << "incorrect\n";
				BHT[Entrada_BHT] = 1;		//Disminuye a Weakly Not Taken
				fallosN++;
			}	
		}
		else if (3 == BHT[Entrada_BHT]){		//Si Strongly Taken
			prediccion = "T";
			file << prediccion << "\t\t\t";
			if (prediccion == cadena){
				file << "correct\n";
				aciertosT++;
				PHT[Entrada_PHT]++;		// Suma 1 si el salto se tomó
			}else{
				file << "incorrect\n";
				BHT[Entrada_BHT] = 2;		//Disminuye a Weakly Taken
				fallosN++;
			}	
		}
	cin >> cadena;		//PC
	}
	file.close();
	
	int Aciertos = aciertosN + aciertosT;
	int Branches = aciertosN + fallosN + aciertosT + fallosT;
	float PorcentajeAciertos = (float(Aciertos)/float(Branches))*100;
	if (o!="1")
		remove("PShare.txt");
	cout << "\n\nPrediction parameters:\nBranch prediction type:\t\t\t\t\tPShare\nBHT size (entries):\t\t\t\t\t" << TamanoBHT << "\nGlobal history register size:\t\t\t\t" << gh << "\nPrivate history register size:\t\t\t\t" << ph <<"\nSimulation results:\nNumber of branch:\t\t\t\t\t" << Branches  << "\nNumber of correct prediction of taken branches:\t\t" << aciertosT << "\nNumber of incorrect prediction of taken branches:\t" << fallosT << "\nCorrect prediction of not taken branches:\t\t" << aciertosN << "\nIncorrect prediction of not taken branches:\t\t" << fallosN << "\nPercentage of correct predictions\t\t\t" << PorcentajeAciertos << "\n\n";
}

//Funcion para el predictor de Torneo: 
//Se utiliza un metapredictor de tamaño 2^s que define cual prediccion usar, si del predictor GShare o de PShare.
//Entradas del MetaPredictor:
//0(00) : Strongly prefer GShare
//1(01) : Weakly prefer GShare
//2(10) : Weakly prefer PShare
//3(11) : Strongly prefer PShare
//Se hacen bloques de código reciclado de GShare y PShare y otros de código propio del predictor de torneo
void Torneo (string s, string gh, string ph, string o){
	unsigned long int TamanoMetapredictor = pow(2,std::stoi(s));
	int Metapredictor [TamanoMetapredictor];
	int i;
	for (i=0; i<TamanoMetapredictor; i++){
		Metapredictor[i]=3;		//Se inicializa en Strongly prefer PShare
	}
	ofstream file;
	file.open("Torneo.txt");
	file << "PC\t\t\tOutcome\tPrediction\tcorrect/incorrect\n";
	int aciertosN = 0;	//Correct prediction of not taken branches:
	int aciertosT = 0;	//Correct prediction of taken branches:
	int fallosN = 0;	//Incorrect prediction of not taken branches:
	int fallosT = 0; 	//Incorrect prediction of taken branches:
	unsigned long int PC;	
	unsigned long int Entrada_Metapredictor;
	string prediccion;
	
	//GShare: Declaraciones e Inicializaciones 
	unsigned long int TamanoHistoria_GH = pow(2,std::stoi(gh))-1;
	unsigned long int Historia_GH =0; // No se trabaja como un array pero va a tener un valor maximo de acuerdo al numero de bits que tenga dado por TamanoHistoria.
	unsigned long int TamanoBHT_GH = pow(2,std::stoi(s));
	int BHT_GH [TamanoBHT_GH]= {};
	Historia_GH = 0;
	unsigned long int Entrada_BHT_GH;
	string prediccion_GH;
	/////////////
	
	//PShare: Declaraciones e Inicializaciones 
	unsigned long int TamanoHistoria_PH = pow(2,std::stoi(ph))-1;
	unsigned long int TamanoBHT_PH = pow(2,std::stoi(s));//Tambien funciona para indexar
	int BHT_PH [TamanoBHT_PH]= {};
	int PHT [TamanoBHT_PH]= {};
	unsigned long int Entrada_BHT_PH;
	unsigned long int Entrada_PHT;
	string prediccion_PH;
	///////////////
	////Torneo
	char cadena [100]="a";
	cin >> cadena;		//PC
	while (!cin.eof()){
		file << cadena << "\t";
		PC=strtoul(cadena,NULL,10);
		//GShare: Operaciones
		Entrada_BHT_GH = ((Historia_GH ^ PC) & (TamanoBHT_GH-1));
		Historia_GH = Historia_GH << 1;
		Historia_GH = Historia_GH & TamanoHistoria_GH;
		//////
		//PShare: Operaciones
		Entrada_PHT = (TamanoBHT_PH-1) & PC;
		Entrada_BHT_PH = (PHT[Entrada_PHT] ^ Entrada_PHT) & (TamanoBHT_PH-1);
		PHT[Entrada_PHT] = PHT[Entrada_PHT] << 1;
		PHT[Entrada_PHT] = PHT[Entrada_PHT] & TamanoHistoria_PH;
		//////
		cin >> cadena;	//Outcome
		file << cadena << "\t\t";
		//GShare: Prediccion
		if (0 == BHT_GH[Entrada_BHT_GH] || 1 == BHT_GH[Entrada_BHT_GH]){
			prediccion_GH = "N";
		}
		else if (2 == BHT_GH[Entrada_BHT_GH] || 3 == BHT_GH[Entrada_BHT_GH]){
			prediccion_GH = "T";
		}
		////////////
		//PShare: Prediccion
		if (0 == BHT_PH[Entrada_BHT_PH] || 1 == BHT_PH[Entrada_BHT_PH]){
			prediccion_PH = "N";
		}
		else if (2 == BHT_PH[Entrada_BHT_PH] || 3 == BHT_PH[Entrada_BHT_PH]){
			prediccion_PH = "T";
		}
		////////////
		//Torneo: Actualizar el Metapredictor
		int Resultado_Metapredictor;
		Entrada_Metapredictor = PC & (TamanoMetapredictor-1);		//Entrada para indexar el metapredictor
		Resultado_Metapredictor = Metapredictor[Entrada_Metapredictor];
		if (Resultado_Metapredictor==0){			//Si Strongly prefer GShare
			file << prediccion_GH << "\t\t\t";
			if (prediccion_GH != cadena & prediccion_PH == cadena)	//Condicion para pasar a Weakly prefer GShare 
				Metapredictor[Entrada_Metapredictor]=1;
		}
		else if (Resultado_Metapredictor==1){		//Si Weakly prefer GShare
			file << prediccion_GH << "\t\t\t";
			if (prediccion_GH == cadena & cadena != prediccion_PH)		//Condicion para pasar a Strongly prefer GShare 
				Metapredictor[Entrada_Metapredictor]=0;
			else if (prediccion_GH != cadena & cadena == prediccion_PH)		//Condicion para pasar a Weakly prefer PShare 
				Metapredictor[Entrada_Metapredictor]=2;
		}
		else if (Resultado_Metapredictor==2){		//Si Weakly prefer PShare
			file << prediccion_PH << "\t\t\t";
			if (prediccion_PH == cadena & prediccion_GH != cadena)		//Condicion para pasar a Strongly prefer PShare 
				Metapredictor[Entrada_Metapredictor]=3;			
			else if (prediccion_PH != cadena & cadena == prediccion_GH)		//Condicion para pasar a Weakly prefer GShare 
				Metapredictor[Entrada_Metapredictor]=1;
		}
		else if (Resultado_Metapredictor==3){		//Si Strongly prefer PShare
			file << prediccion_PH << "\t\t\t";
			if (prediccion_PH != cadena & prediccion_GH == cadena)		//Condicion para pasar a Weakly prefer PShare 
				Metapredictor[Entrada_Metapredictor]=2;
		}	
		
		if ((Resultado_Metapredictor==0) || (Resultado_Metapredictor==1)){	
			//GShare: Cambios en el archivo si el metapredictor decide GShare
			if (0 == BHT_GH[Entrada_BHT_GH] || 1 == BHT_GH[Entrada_BHT_GH]){		//Si fue una prediccion Not Taken
				if (prediccion_GH == cadena){		//Acertó
					file << "correct\n";
					aciertosN++;
				}else{
					file << "incorrect\n";
					fallosT++;
				}	
			}
			else if (2 == BHT_GH[Entrada_BHT_GH] || 3 == BHT_GH[Entrada_BHT_GH]){		//Si fue una prediccion Taken
				if (prediccion_GH == cadena){		//Acertó
					file << "correct\n";
					aciertosT++;
				}else{
					file << "incorrect\n";
					fallosN++;
				}	
			}
			//////////
		}
		else if ((Resultado_Metapredictor==2) || (Resultado_Metapredictor==3)){	
			//PShare: Cambios en el archivo si el metapredictor decide PShare
			if (0 == BHT_PH[Entrada_BHT_PH] || 1 == BHT_PH[Entrada_BHT_PH]){		//Si fue una prediccion Not Taken
				if (prediccion_PH == cadena){		//Acertó
					file << "correct\n";
					aciertosN++;
				}else{
					file << "incorrect\n";
					fallosT++;
				}	
			}
			else if (2 == BHT_PH[Entrada_BHT_PH] || 3 == BHT_PH[Entrada_BHT_PH]){		//Si fue una prediccion Taken
				if (prediccion_PH == cadena){		//Acertó
					file << "correct\n";
					aciertosT++;
				}else{
					file << "incorrect\n";
					fallosN++;
				}	
			}
			//////////////////////////
		}
				
		
		//GShare: Actualizaciones al GShare. Actualizar BHT de acuerdo al salto y la historia global
		if ((0 == BHT_GH[Entrada_BHT_GH]) & (prediccion_GH != cadena)){
			BHT_GH[Entrada_BHT_GH] = 1;
			Historia_GH++;
		}
		else if (1 == BHT_GH[Entrada_BHT_GH]){
			if (prediccion_GH == cadena){
				BHT_GH[Entrada_BHT_GH] = 0;
			}else{
				BHT_GH[Entrada_BHT_GH] = 2;
				Historia_GH++;
			}	
		}
		else if (2 == BHT_GH[Entrada_BHT_GH]){
			if (prediccion_GH == cadena){
				BHT_GH[Entrada_BHT_GH] = 3;
				Historia_GH++;
			}else{
				BHT_GH[Entrada_BHT_GH] = 1;
			}		
		}
		else if (3 == BHT_GH[Entrada_BHT_GH]){
			if (prediccion_GH == cadena){
				Historia_GH++;
			}else{
				BHT_GH[Entrada_BHT_GH] = 2;
			}	
		}
		//////////
		
		//PShare: Actualizaciones al PShare. Actualizar BHT de acuerdo al salto y la historia privada
		if ((0 == BHT_PH[Entrada_BHT_PH]) & (prediccion_PH != cadena)){
			BHT_PH[Entrada_BHT_PH] = 1;
			PHT[Entrada_PHT]++;
		}
		else if (1 == BHT_PH[Entrada_BHT_PH]){
			if (prediccion_PH == cadena){
				BHT_PH[Entrada_BHT_PH] = 0;
			}else{
				BHT_PH[Entrada_BHT_PH] = 2;
				PHT[Entrada_PHT]++;
			}	
		}
		else if (2 == BHT_PH[Entrada_BHT_PH]){
			if (prediccion_PH == cadena){
				BHT_PH[Entrada_BHT_PH] = 3;
				PHT[Entrada_PHT]++;
			}else{
				BHT_PH[Entrada_BHT_PH] = 1;
			}	
		}
		else if (3 == BHT_PH[Entrada_BHT_PH]){
			if (prediccion_PH == cadena){
				PHT[Entrada_PHT]++;
			}else{
				BHT_PH[Entrada_BHT_PH] = 2;
			}	
		}
		//////////////////////////
		cin >> cadena;		//PC siguinte
	}
	file.close();
	
	int Aciertos = aciertosN + aciertosT;
	int Branches = aciertosN + fallosN + aciertosT + fallosT;
	float PorcentajeAciertos = (float(Aciertos)/float(Branches))*100;
	if (o!="1")
		remove("Torneo.txt");
	cout << "\n\nPrediction parameters:\nBranch prediction type:\t\t\t\t\tTorneo\nMetapredictor size (entries):\t\t\t\t" << TamanoMetapredictor << "\nGlobal history register size:\t\t\t\t" << gh << "\nPrivate history register size:\t\t\t\t" << ph <<"\nSimulation results:\nNumber of branch:\t\t\t\t\t" << Branches  << "\nNumber of correct prediction of taken branches:\t\t" << aciertosT << "\nNumber of incorrect prediction of taken branches:\t" << fallosT << "\nCorrect prediction of not taken branches:\t\t" << aciertosN << "\nIncorrect prediction of not taken branches:\t\t" << fallosN << "\nPercentage of correct predictions\t\t\t" << PorcentajeAciertos << "\n\n";
	
}



int main(int argc, char * argv[]){
	string s = argv[2];
	string bp = argv[4];
	string gh = argv[6];
	string ph = argv[8];
	string o = argv[10];
	string bimodal = "Bimodal";
	string HistoriaPrivada = "PH";
	string HistoriaGlobal = "GH";
	string TorneoGP = "Torneo";
	if (argv[4]==bimodal)
		Bimodal(s,gh,ph,o);
	else if (argv[4]==HistoriaGlobal)
		GShare(s,gh,ph,o);
	else if (argv[4]==HistoriaPrivada)
		PShare(s,gh,ph,o);	
	else if (argv[4]==TorneoGP)
		Torneo(s,gh,ph,o);
	else
		cout << "Debe elegir un predictor valido. Opciones: Bimodal, PH, GH o Torneo";
	return 1;                     
}
